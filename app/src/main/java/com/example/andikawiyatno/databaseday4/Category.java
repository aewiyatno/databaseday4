package com.example.andikawiyatno.databaseday4;

/**
 * Created by andikawiyatno on 1/11/18.
 */

public class Category {
    private int _id;
    private String name;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
