package com.example.andikawiyatno.databaseday4;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by andikawiyatno on 1/11/18.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "rspelni.db";
    private static final String TABLE_CATEGORY = "categories";
    private static final String TABLE_DOCTOR = "doctors";
    public static final String COL_ID = "_id";
    public static final String COL_NAME = "name";
    public static final String COL_KET = "keterangan";
    public static final String COL_CAT_ID = "category_id";

    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, DATABASE_NAME,factory,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_TABLE_CATEGORIES = "CREATE TABLE "+TABLE_CATEGORY+
                " ("+COL_ID+" INTEGER PRIMARY KEY, "+ COL_NAME+" TEXT)";
        String CREATE_TABLE_DOCTORS = "CREATE TABLE "+TABLE_DOCTOR+
                " ("+COL_ID+" INTEGER PRIMARY KEY, "+COL_CAT_ID+" INTEGER, "
                +COL_NAME+" TEXT, "+COL_KET+" TEXT)";

        db.execSQL(CREATE_TABLE_CATEGORIES);
        db.execSQL(CREATE_TABLE_DOCTORS);
        this.insertDataCategory(db);
//        this.insertDataDoctor(db);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CATEGORY);
        onCreate(db);
    }

    private void insertDataCategory(SQLiteDatabase db){
        String INSERT_CATEGORY_DATA = "INSERT INTO "+TABLE_CATEGORY+" ("
                +COL_NAME+") VALUES "
                + "(\"PENYAKIT DALAM\"),"
                +  "(\"MATA\"),"
                + "(\"JANTUNG\")";
        db.execSQL(INSERT_CATEGORY_DATA);
        Log.i("Info", "data catalog inserted");
    }
    public Cursor fetchAllCategory(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_CATEGORY, new String[]{COL_ID,COL_NAME},
                null, null,null,null,COL_ID);
        return cursor;
    }
//    private void insertDataDoctor(SQLiteDatabase db){
//        String INSERT_DATA_DOCTOR = "";
//        db.execSQL(INSERT_DATA_DOCTOR);
//    }

}