package com.example.andikawiyatno.databaseday4;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    private DBHandler dbHandler;
    private SimpleCursorAdapter simpleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //declare listview
        ListView listView = (ListView)findViewById(R.id.list_view);
        //declare connection to db
        dbHandler = new DBHandler(this,null,null,1);
        //get curson (content of db)
        Cursor cursor = dbHandler.fetchAllCategory();
        //declare simple adapter
        String[] from = new String[]{dbHandler.COL_NAME};
        int[] to = new int[]{R.id.txt_name};
        simpleAdapter = new SimpleCursorAdapter(this,R.layout.list_item,cursor,from,to,0);

        listView.setAdapter(simpleAdapter);
    }

}
